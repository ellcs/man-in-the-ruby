# frozen_string_literal: true

require 'man_in_the_ruby/usage_storage'

module ManInTheRuby
  module Interceptor
    def intercept
      @record_calls_of_next_definition = true
    end

    def method_added(method)
      @record_calls_of_next_definition ||= false
      if @record_calls_of_next_definition
        Interceptor.intercept(self, method)
        @record_calls_of_next_definition = false
      end
      super
    end

    class << self
      def intercept(klass_to_intecept, method)
        intercept_methods(klass_to_intecept, [method])
      end

      # we can not intercept methods defined in Kernel.
      def intercept_all(klass_to_intecept)
        methods = klass_to_intecept.instance_methods - Kernel.instance_methods
        intercept_missing_method(klass_to_intecept)
        intercept_methods(klass_to_intecept, methods)
      end

      # klass_to_intecept
      # methods to intercept
      #
      # creates an interceptor. its a anonymous module which contains method
      # definitions for all passed methods.
      def intercept_methods(klass_to_intecept, methods)
        interceptor = Module.new do
          methods.each do |method|
            define_method(method) do |*args_array,**args_hash, &block|
              begin
                result = super(*args_array, **args_hash, &block)
                UsageStorage.add_normal_usage(caller, self.class, method, args_array, args_hash, block, result)
              rescue Exception => e
                UsageStorage.add_exception_raised(caller, self.class, method, args_array, args_hash, block, e)
                raise e
              end
              result
            end
          end
        end
        klass_to_intecept.prepend(interceptor)
        interceptor
      end

      # https://stackoverflow.com/questions/32661792/method-missing-doesnt-work-with-prepend
      #
      # no-op if no :missing_method has been defined.
      # renames existing :missing_method and does intercept it.
      def intercept_missing_method(klass_to_intecept)
        return unless klass_to_intecept.instance_methods.include?(:missing_method)

        klass_to_intecept.alias_method(:old_missing_method, :missing_method)
        klass_to_intecept.define_method(:missing_method) do |m, *args_array, **args_hash, &block|
          method = "method_missing/#{m}"
          begin
            result = old_missing_method(m, *args_array, **args_hash, &block)
            UsageStorage.add_normal_usage(self.class, method, args_array, block, result)
          rescue Exception => e
            UsageStorage.add_exception_raised(self.class, method, args_array, block, e)
            raise e
          end
          result
        end
      end
    end
  end
end
