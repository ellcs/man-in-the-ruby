# frozen_string_literal: false

# get all modules
#   Object.constants.map { |c| [c, eval("Object::#{c}")] }.select { |e| e[1].class == Module }
module ManInTheRuby
  module Sorbetify
    class << self
      def interface_for_module(nodule)
        raise "not a module: #{nodule}" unless nodule.is_a? Module
  
        rbi_content = "class #{nodule}\n"
        rbi_content << "  class << self\n"
        rbi_content << stringify(class_methods(nodule), 4)
        rbi_content << "  end\n"
        rbi_content << stringify(instance_methods(nodule), 2)
        rbi_content << 'end'
      end
  
      def interface_for_class(klass)
        raise "not a class: #{klass}" unless klass.is_a? Class
  
        rbi_content = "class #{klass}\n"
        rbi_content << stringify(included_modules_for_class(klass), 2)
        rbi_content << "  class << self\n"
        rbi_content << stringify(class_methods(klass), 4)
        rbi_content << "  end\n"
        rbi_content << stringify(instance_methods(klass), 2)
        rbi_content << 'end'
      end
  
      def stringify(array, indentation)
        padding = ' ' * indentation
        array.map do |line|
          padding + line + "\n"
        end.join
      end
  
      def included_modules_for_class(c)
        ancestors(c).reduce([]) do |accu, ancestor|  
          if ancestor.class == Module
            accu << "include #{ancestor}"
          else
            return accu
          end
        end
      end

      # without klass itself
      def ancestors(klass)
        klass.ancestors - [klass]
      end
  
      def ancestors_instance_methods(klass)
        ancestors(klass).reduce([]) { |a, c| a << c.instance_methods }.flatten! || []
      end
  
      def ancestors_class_methods(klass)
        ancestors(klass).reduce([]) { |a, c| a << c.methods }.flatten! || []
      end
  
      # Class => Array[String]
      def class_methods(klass)
        ancestor_methods = ancestors_class_methods(klass)
        real_klass_methods = klass.methods - ancestor_methods
        real_klass_methods = real_klass_methods.map { |m| klass.method(m) }
        generate_method_rbi(real_klass_methods)
      end
  
      # Class => Array[String]
      def instance_methods(klass)
        ancestor_methods = ancestors_instance_methods(klass)
        real_methods = klass.instance_methods - ancestor_methods
        real_methods = real_methods.map { |m| klass.instance_method(m) }
        generate_method_rbi(real_methods)
      end
  
      # Array[Method] => Array[String]
      def generate_method_rbi(methods)
        methods.map do |method|
          "def #{method.name}#{method_parameters(method)}; end"
        end
      end
  
      # Method => String
      # string might have brackets
      #
      # arity of zero could still have a block parameter
      def method_parameters(method)
        accu = []
        method.parameters.map.with_index do |parameter, index|
          # parameter[0] is parameter type
          # parameter[1] is parameter name, if there is one
          type, original_name = parameter
          name = original_name || ('a'.ord + index).chr
          if type == :req
            accu << name
          elsif type == :opt
            warn("Can not determine optional parameter for method #{method}, paramter: #{name}(#{index})")
            accu << "#{name} = nil"
          elsif type == :rest
            accu << "*#{name}"
          elsif type == :keyrest
            accu << "**#{name}"
          elsif type == :block
            original_name ||= "block"
            accu << "&#{name}"
          end
        end
        "(#{accu.join(', ')})"
      end
  
      def sorbetify(o)
        if o.class == Class
          interface_for_class(o)
        elsif o.class == Module
          interface_for_module(o)
        else
          raise 'please submit a report :)'
        end
      end
  
      def sorbetify_including_ancestors(o)
        o.ancestors.map { |a| sorbetify(a) }
      end
    end
  end
end
