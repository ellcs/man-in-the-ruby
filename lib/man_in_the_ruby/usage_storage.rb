# frozen_string_literal: true

require 'singleton'
require 'ostruct'
require 'set'

class Usage
  attr_reader :type,
              :time,
              :klass,
              :method,
              :args,
              :block,
              :result_or_exception

  def initialize(**hash)
    raise 'pls no' if hash[:result] && hash[:exception] || !(hash[:result] || hash[:exception])

    @time = Time.now
    @klass = hash[:klass]
    @method = hash[:method]
    @args = hash[:args]
    @block = hash[:block]
    @type = :exception_raised if hash[:exception]
    @type = :value_returned if hash[:result]
    @result_or_exception = hash[:result] || hash[:exception]
  end
end

module UsageStorage
  class << self
    @@usages = Set.new

    def reset
      @@usages = Set.new
    end

    def record_scope(&block)
      @@usages_to_restore = @@usages
      @@usages = Set.new
      block.call
      @@usages_to_return = @@usages
      @@usages = @@usages_to_restore
    end

    def add_normal_usage(klass, method, args, block, result)
      @@usages << Usage.new(klass: klass,
                            method: method,
                            args: args,
                            block: block,
                            result: result)
    end

    def add_exception_raised(klass, method, args, block, exception)
      @@usages << Usage.new(klass: klass,
                            method: method,
                            args: args,
                            block: block,
                            exception: exception)
    end

    def usages
      @@usages.clone
    end

    def reset_usage
      @@usages = Set.new
    end
  end
end
