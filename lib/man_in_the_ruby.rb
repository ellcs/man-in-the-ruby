# frozen_string_literal: true $LOAD_PATH.unshift File.join(File.dirname(__FILE__))

require 'man_in_the_ruby/usage_storage'
require 'man_in_the_ruby/interceptor'
require 'man_in_the_ruby/sorbetify'

module ManInTheRuby
  class << self
    def intercept(object, method)
      class_to_intecept = if object.is_a?(Class)
                            object
                          else
                            object.singleton_class
                          end
      Interceptor.intercept(class_to_intecept, method)
    end

    def intercept_all(object)
      class_to_intecept = if object.is_a?(Class)
                            object
                          else
                            object.singleton_class
                          end
      Interceptor.intercept_all(class_to_intecept)
    end

    def intercepted_messages
      UsageStorage.usages
    end

    def destroy_intercepted_messages!
      UsageStorage.reset
    end

    def sorbetify(obj)
      Sorbetify.sorbetify(obj)
    end
  end
end

MITR = ManInTheRuby
