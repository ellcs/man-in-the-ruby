# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name        = 'man_in_the_ruby'
  s.version     = '0.0.0'
  s.date        = '2018-12-18'
  s.summary     = 'Intercept method calls, to listen what objects tell each other.'
  s.description = 'Intercept method calls, to listen what objects tell each other.'
  s.authors     = ['ellcs (Alex Mantel)']
  s.email       = ''
  s.files       = ['lib/man_in_the_ruby.rb',
                   'lib/man_in_the_ruby/interceptor.rb',
                   'lib/man_in_the_ruby/sorbetify.rb',
                   'lib/man_in_the_ruby/usage_storage.rb']
  s.homepage    = ''
  s.license     = 'GPL-3.0'
end
