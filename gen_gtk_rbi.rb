require 'gtk3'
require 'man_in_the_ruby'

# load binding completly :)
Gtk::Button

BASE_DIR = "tmp"

module RubyMeta
  class << self
    def all_classes_in(object)
      raise "nope" unless [Class, Module].include?(object.class)
      all_constants_in(object).select { |c| c.class == Class }
    end
    
    def all_modules_in(object)
      raise "nope" unless [Class, Module].include?(object.class)
      all_constants_in(object).select { |c| c.class == Module }
    end
    
    def all_constants_in(object)
      raise "nope" unless [Class, Module].include?(object.class)
      object.constants.map do |constant|
        object.const_get(constant)
      end
    end
    
    def snake_case(str)
      str.gsub(/::/, '/').
        gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
        gsub(/([a-z\d])([A-Z])/,'\1_\2').
        tr("-", "_").
        downcase
    end
  end
end

def generate_rbi(o)
  filename = RubyMeta.snake_case(o.to_s)
  File.open("#{BASE_DIR}/#{filename}.rb", "w") do |file|
    file.write(ManInTheRuby.sorbetify(o))
  end
end

generate_rbi(Gtk)
RubyMeta.all_modules_in(Gtk).map do |mod|
  generate_rbi(mod)
end
RubyMeta.all_classes_in(Gtk).map do |c|
  generate_rbi(c)
end
