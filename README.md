# Man In The Ruby
You know how it is. The code you currently look at hasn't been modified, since Columbus arrived north America and was about to enslave the continent. After crying in front of the whole office, your boss finally agrees to give you some time for refactoring work. But guess what, refactoring legacy code isn't a simple task and one day after your first merge, your colleagues get mad at you, because you broke the code. Of course you start to google (if you are fancy duckduckgo or searx.me) for some tools to refactor. You didn't find something useful. And finally you're here!

But guess what! Your intentions are great! And in my eyes it's totaly fine to show your emotions. Heads up! :)

I hope this gem helps you to write unit tests, by intercepting the messages sent between ruby objects. 


Altough i am quite sure you heard about man-in-the-middle attacks. I give you a brief introduction. 

- Intercept messages
- Modify messages
- Generate sorbet signatures