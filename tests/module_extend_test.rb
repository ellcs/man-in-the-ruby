# frozen_string_literal: true

require './tests/test_helper'
require 'man_in_the_ruby'

class TestModuleExtend < Minitest::Test
  module ModuleToRecord
    extend ManInTheRuby::Interceptor

    intercept
    def test(a, b, c)
      a + b + c
    end
  end

  class Usecase
    include ModuleToRecord
  end

  def test_add_normal_usage
    ManInTheRuby.destroy_intercepted_messages!
    Usecase.new.test(1, 2, 3)
    Usecase.new.test(1, 2, 3) { puts('hi') }
    Usecase.new.test('', 'asdf', 'qwer')

    assert_equal 3, ManInTheRuby.intercepted_messages.count
  end
end
