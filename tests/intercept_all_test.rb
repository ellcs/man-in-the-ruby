# frozen_string_literal: true

require './tests/test_helper'
require 'man_in_the_ruby'

class TestInterceptAll < Minitest::Test
  class DefinedMethods
    def first(f)
      f
    end

    def second(s)
      s
    end

    def third(t)
      t
    end
  end

  def test_all_defined_methods_intercepted
    ManInTheRuby.destroy_intercepted_messages!
    defined_methods = DefinedMethods.new
    ManInTheRuby.intercept_all(defined_methods)
    defined_methods.first(1)
    defined_methods.second(2)
    defined_methods.third(3)
    assert_equal 3, ManInTheRuby.intercepted_messages.count
  end

  def test_intercept_all__does_not_record_missing_methods_if_not_defined
    assert_raises(NoMethodError) do
      defined_methods = DefinedMethods.new
      ManInTheRuby.intercept_all(defined_methods)
      defined_methods.i_am_missing(:asdf, :asdf)
    end
  end

  class DefinedMissingMethod
    def method_missing(m, *args)
      joined_args = args.map(&:to_s).join(', ')
      "you called: #{m}(#{joined_args})"
    end
  end

  def test_intercept_all__records_missing_method
    ManInTheRuby.destroy_intercepted_messages!
    defined_methods = DefinedMissingMethod.new
    ManInTheRuby.intercept_all(defined_methods)
    defined_methods.i_am_missing(:asdf, :asdf)
    assert_equal 1, ManInTheRuby.intercepted_messages.count
  end
end
