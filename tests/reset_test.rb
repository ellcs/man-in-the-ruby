# frozen_string_literal: true

require './tests/test_helper'
require 'man_in_the_ruby'

class TestReset < Minitest::Test
  class Thing
    def method
      :thing
    end
  end

  def test_reset
    # fill
    ManInTheRuby.intercept(Thing, :method)
    t = Thing.new
    t.method
    refute_empty ManInTheRuby.intercepted_messages
    ManInTheRuby.destroy_intercepted_messages!
    assert_empty ManInTheRuby.intercepted_messages
  end
end
