# frozen_string_literal: true

require './tests/test_helper'
require 'man_in_the_ruby'

class TestSimpleSorbetify < Minitest::Test
  class Foo
    def splat(*args); end
    def simple_args(first, second, third); end
    def restargs(**args); end
    def block(&block); end
  end

  def test_foo
    result = Sorbetify.sorbetify(Foo)
    assert_match "class #{self.class}::Foo", result
    assert_match "  def splat(*args); end", result
    assert_match "  def simple_args(first, second, third); end", result
    assert_match "  def restargs(**args); end", result
    assert_match "  def block(&block); end", result
    assert_match "end", result
  end


  def test_interger
    result = Sorbetify.sorbetify(Integer)
    assert_match "class Integer", result
    assert_match "  class << self", result
    assert_match "    def sqrt(a); end", result
    assert_match "  end", result
    assert_match "  def -(a); end", result
    assert_match "  def +(a); end", result
    assert_match "end", result
  end
end
