# frozen_string_literal: true

require './tests/test_helper'
require 'man_in_the_ruby'

class TestModuleExtend < Minitest::Test
  class A
    def echo(msg)
      "you sent: #{msg}"
    end
  end

  def test_intercept_class
    ManInTheRuby.destroy_intercepted_messages!
    ManInTheRuby.intercept(A, :echo)
    a = A.new
    a.echo('hey')
    refute_empty ManInTheRuby.intercepted_messages
  end

  def test_intercept_object
    ManInTheRuby.destroy_intercepted_messages!
    a = A.new
    ManInTheRuby.intercept(a, :echo)
    a.echo('hey')
    refute_empty ManInTheRuby.intercepted_messages
  end
end
