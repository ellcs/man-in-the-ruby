# frozen_string_literal: true

require './tests/test_helper'
require 'man_in_the_ruby'

class TestAddsExceptions < Minitest::Test
  class Usecase
    extend ManInTheRuby::Interceptor

    intercept
    def test(a, b, c)
      a + b + c
    end
  end

  def test_add_normal_usage
    ManInTheRuby.destroy_intercepted_messages!
    begin
      Usecase.new.test('', 1, '')
    rescue StandardError
      nil
    end
    assert_equal 1, ManInTheRuby.intercepted_messages.count
  end
end
